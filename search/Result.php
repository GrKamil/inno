<?php

namespace app\search;


use app\search\interfaces\ResultInterface;

class Result implements ResultInterface, \JsonSerializable
{

    private $data = [
        'sort' => null,
        'direction' => null,
        'items' => [],
        'count' => null,
        'totalCount' => null,
        'filters' => [],
        'offset' => null,
        'time' => null
    ];


    public function setItems($items)
    {
        $this->data['items'] = $items;

        return $this;
    }

    public function setCount($count)
    {
        $this->data['count'] = (int) $count;

        return $this;
    }

    public function setSort($field, $direction)
    {
        $this->data['sort'] = $field;
        $this->data['direction'] = $direction;

        return $this;
    }

    public function setFilters($filters)
    {
        $this->data['filters'] = $filters;

        return $this;
    }

    public function setTotalCount($count)
    {
        $this->data['totalCount'] = (int) $count;

        return $this;
    }

    public function setOffset($offset)
    {
        $this->data['offset'] = (int) $offset;

        return $this;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return $this->data;
    }

    public function setTime($time)
    {
        $this->data['time'] = (float) $time;

        return $this;
    }
}

<?php

namespace app\search;


use app\search\interfaces\ResultItemInterface;

class ResultItem implements ResultItemInterface, \JsonSerializable
{
    private $data;

    public function setData($data) {
        $this->data = $data;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return $this->data;
    }
}

<?php

namespace app\search;


use app\search\interfaces\SearchEngineInterface;
use yii\sphinx\Query;

class SearchEngine implements SearchEngineInterface
{

    private $q;
    private $sortField;
    private $sortDirection;
    private $limit;
    private $offset;
    private $filters = [];

    public function setQuery($q)
    {
        $this->q = $q;
        return $this;
    }

    public function search()
    {
        $result = new Result();

        $query = new Query();
        $query->from('resume')
            ->match($this->q)
            ->showMeta(true);

        if($this->limit) {
            $query->limit($this->limit);
        }

        if($this->offset) {
            $query->offset($this->offset);
        }

        if($this->sortDirection) {
            $query->orderBy([
                $this->sortField => $this->sortDirection === 'desc' ? SORT_DESC : SORT_ASC
            ]);
        }

        $sphinxResult = $query->search();

        $items = [];

        foreach ($sphinxResult['hits'] as $row) {
            $item = new ResultItem();
            $item->setData([
                'id' => $row['id']
            ]);

            $items[] = $item;
        }

        $result->setItems($items);

        $result->setSort($this->sortField, $this->sortDirection);
        $result->setOffset($this->offset);
        $result->setFilters($this->filters);

        $result->setCount(count($items));
        $result->setTotalCount($sphinxResult['meta']['total_found']);
        $result->setTime($sphinxResult['meta']['time']);

        return $result;
    }

    public function setFilters($filters)
    {
        $this->filters = $filters;

        return $this;
    }

    public function setSort($field, $direction)
    {
        $this->sortField = $field;
        $this->sortDirection = strtolower($direction);
        
        return $this;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;
        
        return $this;
    }

    public function setOffset($offset)
    {
        $this->offset = $offset;

        return $this;
    }
}

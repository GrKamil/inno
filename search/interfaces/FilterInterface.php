<?php

namespace app\search\interfaces;

interface FilterInterface
{
    public function setField($field);

    public function setValue($value);
}
<?php

namespace app\search\interfaces;

interface ResultInterface
{
    public function setItems($items);

    public function setCount($count);

    public function setTotalCount($count);

    public function setOffset($offset);

    public function setSort($field, $direction);

    public function setFilters($filters);
}
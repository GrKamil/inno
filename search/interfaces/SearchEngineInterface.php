<?php

namespace app\search\interfaces;

interface SearchEngineInterface
{
    public function setQuery($q);

    public function search();

    public function setFilters($filters);

    public function setSort($field, $direction);

    public function setLimit($limit);

    public function setOffset($offset);
}
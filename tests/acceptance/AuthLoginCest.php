<?php


class AuthLoginCest
{
    public function _before(AcceptanceTester $I)
    {
        $I->amOnUrl('https://stage.career.innopolis.ru');
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function tryIncorrectCredentials(AcceptanceTester $I)
    {
        $I->amOnPage('/session/login');
        $I->see('Вход');

        $I->amGoingTo('try to login with incorrect credentials');
        $I->fillField('input[name="LoginForm[email]"]', 'grkamil@yandex.ru');
        $I->fillField('input[name="LoginForm[password]"]', '123');
        $I->click('Войти');
        $I->expect('the form is not submitted');
        $I->wait(2);
        $I->see('Неверные имя пользователя или пароль');
    }

    // tests
    public function tryCorrectCredentials(AcceptanceTester $I)
    {
        $I->amOnPage('/session/login');
        $I->see('Вход');

        $I->amGoingTo('try to login with correct credentials');
        $I->fillField('input[name="LoginForm[email]"]', 'grkamil@yandex.ru');
        $I->fillField('input[name="LoginForm[password]"]', '123456');
        $I->click('Войти');
        $I->wait(2);
        $I->see('Профиль');
        $I->see('Резюме');
    }
}

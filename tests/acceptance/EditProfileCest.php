<?php


class EditProfileCest
{
    const LOGIN = 'grkamil@yandex.ru';
    const PASSWORD = '123456';
    const NEW_PASSWORD = '1234567';

    public function _before(AcceptanceTester $I)
    {
        $I->amOnUrl('https://stage.career.innopolis.ru');
        $I->amOnPage('/session/login');
        $I->fillField('input[name="LoginForm[email]"]', self::LOGIN);
        $I->fillField('input[name="LoginForm[password]"]', self::PASSWORD);
        $I->click('Войти');
        $I->wait(1);
        $I->amOnPage('/profile/settings');
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function testIncorrectOldPassword(AcceptanceTester $I)
    {
        $I->fillField('SettingsForm[oldPassword]', '123');
        $I->fillField('SettingsForm[password]', '123456');
        $I->fillField('SettingsForm[comparePassword]', '123456');
        $I->click('Сохранить');
        $I->waitForText('Введен неверный текущий пароль', 5);
    }

    public function testDifferentNewPassword(AcceptanceTester $I)
    {
        $I->fillField('SettingsForm[oldPassword]', self::PASSWORD);
        $I->fillField('SettingsForm[password]', '123456');
        $I->fillField('SettingsForm[comparePassword]', '12345');
        $I->click('Сохранить');
        $I->waitForText('Значение «Повторите пароль» должно быть равно «Новый пароль»', 5);
    }

    public function testShortNewPassword(AcceptanceTester $I)
    {
        $I->fillField('SettingsForm[oldPassword]', self::PASSWORD);
        $I->fillField('SettingsForm[password]', '12345');
        $I->fillField('SettingsForm[comparePassword]', '12345');
        $I->click('Сохранить');
        $I->waitForText('Значение «Новый пароль» должно содержать минимум 6 символов.', 5);
    }

    public function testChangingPassword(AcceptanceTester $I)
    {
        $I->fillField('SettingsForm[oldPassword]', self::PASSWORD);
        $I->fillField('SettingsForm[password]', self::NEW_PASSWORD);
        $I->fillField('SettingsForm[comparePassword]', self::NEW_PASSWORD);
        $I->click('Сохранить');
        $I->waitForText('Информация успешно сохранена', 5);

        $I->fillField('SettingsForm[oldPassword]', self::NEW_PASSWORD);
        $I->fillField('SettingsForm[password]', self::PASSWORD);
        $I->fillField('SettingsForm[comparePassword]', self::PASSWORD);
        $I->click('Сохранить');
        $I->waitForText('Информация успешно сохранена', 5);
    }
}

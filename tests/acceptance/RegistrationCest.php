<?php


class RegistrationCest
{
    public function _before(AcceptanceTester $I)
    {
        $I->amOnUrl('https://stage.career.innopolis.ru');
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function tryIncorrectData(AcceptanceTester $I)
    {
        $I->amOnPage('/applicant/signup');

        $I->click('Зарегистрироваться');
        $I->wait(1);
        $I->seeCurrentUrlEquals('/applicant/signup');

        $I->selectOption('SignUpForm[organizationId]', rand(1,3));
        $I->click('Зарегистрироваться');
        $I->wait(1);
        $I->seeCurrentUrlEquals('/applicant/signup');

        $I->fillField('SignUpForm[surname]','test');
        $I->click('Зарегистрироваться');
        $I->wait(1);
        $I->seeCurrentUrlEquals('/applicant/signup');

        $I->fillField('SignUpForm[name]','test');
        $I->click('Зарегистрироваться');
        $I->wait(1);
        $I->seeCurrentUrlEquals('/applicant/signup');

        $I->fillField('SignUpForm[email]','test@test.ru');
        $I->click('Зарегистрироваться');
        $I->wait(1);
        $I->seeCurrentUrlEquals('/applicant/signup');

        $I->fillField('SignUpForm[password]','123456');
        $I->click('Зарегистрироваться');
        $I->wait(1);
        $I->seeCurrentUrlEquals('/applicant/signup');
    }

    public function tryCorrectData(AcceptanceTester $I)
    {
        $I->amOnPage('/applicant/signup');

        $I->selectOption('SignUpForm[organizationId]', rand(1,3));
        $I->fillField('SignUpForm[surname]','test');
        $I->fillField('SignUpForm[name]','test');
        $I->fillField('SignUpForm[email]','test' . rand(1,100) . '@test.ru');
        $I->fillField('SignUpForm[password]','123456');
        $I->click('.cbx-container');
        $I->wait(1);
        $I->click('Зарегистрироваться');
        $I->wait(1);
        $I->seeCurrentUrlEquals('/profile');
    }
}
